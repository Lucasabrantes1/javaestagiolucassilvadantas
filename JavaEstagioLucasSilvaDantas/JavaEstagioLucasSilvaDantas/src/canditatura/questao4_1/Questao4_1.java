package canditatura.questao4_1;
public class Questao4_1 {
	int dia;
	int mes;
	int ano;

	public Questao4_1(int dia, int mes, int ano) {
		this.dia = dia;
		this.mes = mes;
		this.ano = ano;
	}

	public boolean validaData() {

		if(ano >= 0) {

			if((mes >= 1) && (mes <=12)) {

				if(mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {
					if (dia >= 1 && dia <= 31) {
						return true;
					}
					else {
						return false;
					}
				}
				else if(mes == 4 || mes == 6 || mes == 9 || mes == 11) {

					if (dia >= 1 && dia <= 30) {
						return true;
					}
					else {
						return false;
					}
				}
				else {
				
					if(ano % 4 != 0) { 

						if(dia >= 1 && dia <= 28) {
							return true;
						} else {
							return false;
						}
					}
					else if(ano % 4 == 0) {

						if(ano % 100 != 0) {

							if(dia >= 1 && dia <= 29) {
								return true;
							}
							else {
								return false;
								}
						}
						else {

							if(ano % 400 == 0) {
								return true;
							}
							else {
								return false;
							}
						}
					}
				} 
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
		return true;
	}


	public void mostrarData() {
		if(validaData()) {
			System.out.println(this.dia+"/"+this.mes+"/"+this.ano);	
		}
		else {
			System.out.println("Data n�o � v�lida.");
		}
	}

	public static void main(String[] args) {

		Questao4_1 data = new Questao4_1(30,2,2016);
		data.validaData();
		data.mostrarData();
	} 
}